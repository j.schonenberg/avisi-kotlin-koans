package maze


interface MazeGenerator {

    fun generateMaze(width: Int, height: Int): String

}
