# BSN Elfproef

Iedereen dit in Nederland staat ingeschreven heeft een BSN: een uniek persoonsgebonden identificatienummer.

Het BSN voldoet aan een variant van de zogenaamde elfproef: een test om te controleren of een gegeven BSN een geldig BSN kan zijn. 
Door middel van deze elfproef wordt de kans op het foutief ingevoeren van een BSN door typfouten beperkt.

De elfproef voor het BSN is als volgt:

Als het BSN wordt voorgesteld door ABCDEFGHI, dan moet
```
(9 × A) + (8 × B) + (7 × C) + (6 × D) + (5 × E) + (4 × F) + (3 × G) + (2 × H) + (-1 × I)
```
een veelvoud van 11 zijn.

Nieuwe BSNs bestaan uit negen cijfers, al zijn vroeger ook achtcijferige BSNs uitgegeven.
Voor veel praktische doeleinden, waaronder de elfproef, wordt aan deze laatste variant een voorloopnul om het negencijferig te maken.

## Oefening 1

* Implementeer `BsnValidator.isValid` zodat in deze functie de validiteit van een ingevoerd BSN wordt gecontroleerd.
* Zorg ervoor dat alle unit tests slagen
