# FizzBuzz

Fizzbuzz is een spelletje voor kinderen waarmee ze de tafels van vermenigvuldiging kunnen leren.
Spelers tellen beurtelings op, en vervangen elk getal dat deelbaar is door 3 met "Fizz", en elk nummer dat deelbaar is door 5 met "Buzz". 
Getallen die deelbaar zijn door zowel 3 als 5 worden vervangen door beide woorden: "Fizzbuzz"!  

Dus, bijvoorbeeld:

`1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, FizzBuzz`

## Oefening 1

Leer de computer Fizzbuzz te spelen. 
Invoer is het start- en eindgetal. 
Uitvoer is het resulterende spel FizzBuzz, volgens het format dat hierboven wordt gebruikt.

* Implementeer de `play`-functie in `FizzBuzz.kt`.
* Zorg ervoor dat alle unit-tests slagen, en breidt de testen uit waar nodig

## Oefening 2

Breidt het spel uit zodat elk getal dat deelbaar is door 7 wordt vervangen door Jazz. 
Bijv: 21 is deelbaar door zowel 3 als 7, en wordt dus vervangen door FizzJazz.

## Oefening 3

Werkt je code ook als het spel in omgekeerde richting (van 20 naar 1) wordt gespeeld?

## Oefening 4

Pas de `FizzBuzz`-class aan zodat de regels configurabel zijn.

* In de configuratie moeten de speciale delers (3, 5, 7) en bijbehorende vervangende woorden (Fizz, Buzz, Jazz) kunnen worden geconfigureerd.
* De default configuratie moet die van het 'klassieke' FizzBuzz zijn: 3 wordt vervangen door Fizz en 5 wordt vervangen door Buzz. 
