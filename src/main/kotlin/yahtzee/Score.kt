package yahtzee


data class Score(
        val ones: Int,
        val twos: Int,
        val threes: Int,
        val fours: Int,
        val fives: Int,
        val sixes: Int,
        val threeOfAKind: Int,
        val fourOfAKind: Int,
        val fullHouse: Int,
        val smallStraight: Int,
        val largeStraight: Int,
        val yahtzee: Int,
        val chance: Int
)

