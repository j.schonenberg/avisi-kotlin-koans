# Yahtzee

Yahtzee is een dobbelspel waarbij spelers om de beurt met 5 dobbelstenen werpen.

Na elke worp kiest de speler in welke van de dertien categorieën hij zijn geworpen combinatie wil onderbrengen, waarmee hij (hopelijk) punten scoort. Hoeveel punten dit zijn ligt aan de
geworpen dobbelstenen en de gekozen categorie.

De puntentelling van de dertien categorieën is als volgt:

| Categorie | Score | Voorwaarde |
|-----------|-------|------------|
|Enen | Het aantal keer dat 1 geworpen is, maal 1|-|
|Tweeën | Het aantal keer dat twee geworpen is, maal 2|-|
|Drieën | Het aantal keer dat drie geworpen is, maal 3|-|
|Vieren | Het aantal keer dat vier geworpen is, maal 4|-|
|Vijven | Het aantal keer dat vijf geworpen is, maal 5|-|
|Zessen | Het aantal keer dat zes geworpen is, maal 6|-|
|Drie gelijke | Het totaal van alle ogen|Minstens 3 dobbelstenen hetzelfde aantal ogen|
|Vier gelijke | Het totaal van alle ogen|Minstens 4 dobbelstenen hetzelfde aantal ogen|
|Full House | 25 punten| 3 gelijke dobbelstenen en één paar|
| Kleine straat | 30 punten| 4 opeenvolgende ogenaantallen (ongeacht volgorde)|
| Grote straat | 40 punten| 5 opeenvolgende ogenaantallen (ongeacht volgorde)|
| Yahtzee | 50 punten|Alle dobbelstenen hetzelfde aantal ogen|
| Kans | Het totaal van alle ogen|nvt|

## Oefening 1

De ScoreService heeft een methode `calcScoreOptions`. 
Deze methode berekent, gegeven vijf geworpen dobbelstenen, wat de potentiële score in elk van de dertien categorieën is.

* Implementeer de `calcScoreOptions`-methode
* Zorg ervoor dat alle unit tests slagen
