# WerkdagenService

Wanneer een notaris een akte opstelt moet deze binnen een bepaald aantal werkdagen worden ingeschreven bij het Centraal Digitaal Repertorium: het centrale akteregistratiesysteem van de KNB.  

Om de notaris op de hoogte te brengen van de uiterste datum van inschrijving heeft de WerkdagenService een methode om een x-aantal werkdagen bij een gegeven datum op te tellen.   

## Opdracht 1

* Implementeer de `plusWerkdagen`-functie in de `WerkdagenService`
* Zorg ervoor dat alle unit-tests slagen

## Opdracht 2

Ook sommige feestdagen worden niet als werkdag aangemerkt.

* Breidt de `WerkdagenService` uit zodat geconfigureerd kan worden welke feestdagen er zijn. (bijv. 25-12-2021, 26-12-2021, en 01-01-2022) 
* Zorg ervoor dat de `plusWerkdagen`-functie de geconfigureerde feestdagen niet meer als werkdag telt

