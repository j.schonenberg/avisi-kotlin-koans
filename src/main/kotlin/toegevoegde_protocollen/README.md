# Toegevoegde Protocollen

Elke akte die een notaris opstelt schrijft hij in op een zogenaamd protocol. Elk protocol heeft een uniek nummer.

Soms wordt door de KNB besloten om een protocol op te heffen. 
Dit kan echter niet zomaar, want er moet altijd een notaris verantwoordelijk blijven voor de in het verleden ingeschreven akten.
 
Daarom in het geval van opheffing een protocol toegevoegd aan een ander protocol. 
Hiermee wordt ook de verantwoordelijkheid voor het toegevoegde protocol overgedragen aan de notaris die actief is voor het protocol waaraan wordt toegevoegd.
Na verloop van tijd kan zo een lange reeks van toevoegingen ontstaan. 

## Oefening 1

Voor sommige PEC-applicaties is het van belang te weten welke protocollen er, direct of indirect, aan een actief protocol zijn toegevoegd.
Hiermee kan namelijk worden bepaald voor welke akte er verantwoordelijkheid wordt gedragen.

* Implementeer de functie `getToegevoegdeProtocolnummers`: gegeven een `Protocol` geeft deze functie de protocolnummers terug van alle protocollen die direct of indirect aan dit protocol zijn toegevoegd.
