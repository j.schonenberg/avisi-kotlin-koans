package toegevoegde_protocollen


data class Protocol(
        val protocolnummer: Int,
        val toegevoegdeProtocollen: List<Protocol> = emptyList()
)

