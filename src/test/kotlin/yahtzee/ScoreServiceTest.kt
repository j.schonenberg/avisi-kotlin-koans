package yahtzee

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class ScoreServiceTest {

    val scoreService = ScoreService()

    @Test
    fun `test calculate score - sums`() {
        val scoreA = scoreService.calcScoreOptions(1, 2, 3, 4, 5)
        assertSums(scoreA, 1, 2, 3, 4, 5, 0)
        assertScore(scoreA, { chance }, 15)

        val scoreB = scoreService.calcScoreOptions(3, 3, 3, 4, 5)
        assertSums(scoreB, 0, 0, 9, 4, 5, 0)
        assertScore(scoreB, { chance }, 18)

        val scoreC = scoreService.calcScoreOptions(1, 1, 5, 6, 2)
        assertSums(scoreC, 2, 2, 0, 0, 5, 6)
        assertScore(scoreC, { chance }, 15)

        val scoreD = scoreService.calcScoreOptions(6, 6, 2, 2, 2)
        assertSums(scoreD, 0, 6, 0, 0, 0, 12)
        assertScore(scoreD, { chance }, 18)

        val scoreE = scoreService.calcScoreOptions(1, 1, 1, 1, 1)
        assertSums(scoreE, 5, 0, 0, 0, 0, 0)
        assertScore(scoreE, { chance }, 5)
    }

    @Test
    fun `test calculate score - three of a kind`() {
        val threeTimesFour = scoreService.calcScoreOptions(4, 2, 6, 4, 4)
        assertScore(threeTimesFour, { threeOfAKind }, 20)

        val fourTimesSix = scoreService.calcScoreOptions(6, 6, 3, 6, 6)
        assertScore(fourTimesSix, { threeOfAKind }, 27)

        val fiveTimesTwo = scoreService.calcScoreOptions(2, 2, 2, 2, 2)
        assertScore(fiveTimesTwo, { threeOfAKind }, 10)

        val twoPairs = scoreService.calcScoreOptions(1, 1, 5, 5, 6)
        assertScore(twoPairs, { threeOfAKind }, 0)
    }

    @Test
    fun `test calculate score - four of a kind`() {
        val fourTimesOne = scoreService.calcScoreOptions(1, 1, 1, 4, 1)
        assertScore(fourTimesOne, { fourOfAKind }, 8)

        val fiveTimesSix = scoreService.calcScoreOptions(6, 6, 6, 6, 6)
        assertScore(fiveTimesSix, { fourOfAKind }, 30)

        val threeTimesFive = scoreService.calcScoreOptions(5, 5, 2, 5, 6)
        assertScore(threeTimesFive, { fourOfAKind }, 0)
    }

    @Test
    fun `test calculate score - full house`() {
        val threeAndFive = scoreService.calcScoreOptions(3, 5, 3, 5, 3)
        assertScore(threeAndFive, { fullHouse }, 25)

        val oneAndFour = scoreService.calcScoreOptions(1, 4, 4, 4, 1)
        assertScore(oneAndFour, { fullHouse }, 25)

        val allSixes = scoreService.calcScoreOptions(6, 6, 6, 6, 6)
        assertScore(allSixes, { fullHouse }, 0)

        val threeSixes = scoreService.calcScoreOptions(1, 6, 6, 6, 2)
        assertScore(threeSixes, { fullHouse }, 0)

        val twoPairs = scoreService.calcScoreOptions(1, 1, 2, 4, 4)
        assertScore(twoPairs, { fullHouse }, 0)
    }

    @Test
    fun `test calculate score - small straight`() {
        val smallStraightFromOne = scoreService.calcScoreOptions(1, 2, 3, 4, 4)
        val smallStraightFromTwo = scoreService.calcScoreOptions(2, 3, 4, 5, 5)
        val smallStraightFromThree = scoreService.calcScoreOptions(3, 4, 5, 6, 6)

        assertScore(smallStraightFromOne, { smallStraight }, 30)
        assertScore(smallStraightFromTwo, { smallStraight }, 30)
        assertScore(smallStraightFromThree, { smallStraight }, 30)

        val brokenSmallStraight = scoreService.calcScoreOptions(1, 2, 4, 5, 6)
        assertScore(brokenSmallStraight, { smallStraight }, 0)
    }

    @Test
    fun `test calculate score - large straight`() {
        val largeStraightFromOne = scoreService.calcScoreOptions(1, 2, 3, 4, 5)
        val largeStraightFromTwo = scoreService.calcScoreOptions(2, 3, 4, 5, 6)

        assertScore(largeStraightFromOne, { largeStraight }, 40)
        assertScore(largeStraightFromTwo, { largeStraight }, 40)

        val brokenLargeStraight = scoreService.calcScoreOptions(1, 2, 3, 5, 6)
        assertScore(brokenLargeStraight, { largeStraight }, 0)
    }

    @Test
    fun `test calculate score - yahtzee`() {
        val yahtzeeFromFives = scoreService.calcScoreOptions(5, 5, 5, 5, 5)
        assertScore(yahtzeeFromFives, { yahtzee }, 50)

        val almostYahtzee = scoreService.calcScoreOptions(5, 5, 5, 5, 1)
        assertScore(almostYahtzee, { yahtzee }, 0)
    }

    @Test
    fun `test calculate score - chance`() {
        val scoreA = scoreService.calcScoreOptions(1, 2, 3, 4, 5)
        assertScore(scoreA, { chance }, 15)

        val scoreB = scoreService.calcScoreOptions(3, 3, 3, 4, 5)
        assertScore(scoreB, { chance }, 18)

        val scoreC = scoreService.calcScoreOptions(1, 1, 5, 6, 2)
        assertScore(scoreC, { chance }, 15)

        val scoreD = scoreService.calcScoreOptions(6, 6, 2, 2, 2)
        assertScore(scoreD, { chance }, 18)

        val scoreE = scoreService.calcScoreOptions(1, 1, 1, 1, 1)
        assertScore(scoreE, { chance }, 5)
    }

    private fun assertSums(score: Score, scoreOnes: Int, scoreTwos: Int, scoreThrees: Int, scoreFours: Int, scoreFives: Int, scoreSixes: Int) {
        assertScore(score, { ones }, scoreOnes)
        assertScore(score, { twos }, scoreTwos)
        assertScore(score, { threes }, scoreThrees)
        assertScore(score, { fours }, scoreFours)
        assertScore(score, { fives }, scoreFives)
        assertScore(score, { sixes }, scoreSixes)
    }

    private fun assertScore(score: Score, field: Score.() -> Int, expectedValue: Int) {
        assertEquals(expectedValue, score.field())
    }

}
