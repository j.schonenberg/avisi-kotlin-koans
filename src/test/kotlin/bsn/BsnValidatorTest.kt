package bsn

import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class BsnValidatorTest {

    private val bsnValidator = BsnValidator()

    @Test
    fun `test isValid for valid 9 digit BSNs`() {
        assertTrue(bsnValidator.isValid("111222333"))
        assertTrue(bsnValidator.isValid("123456782"))
        assertTrue(bsnValidator.isValid("208706689"))
        assertTrue(bsnValidator.isValid("550151953"))
        assertTrue(bsnValidator.isValid("369647592"))
    }

    @Test
    fun `test isValid for valid 8 digit BSNs`() {
        assertTrue(bsnValidator.isValid("12222343"))
        assertTrue(bsnValidator.isValid("23456784"))
        assertTrue(bsnValidator.isValid("24918386"))
        assertTrue(bsnValidator.isValid("50152956"))
        assertTrue(bsnValidator.isValid("69547592"))
    }

    @Test
    fun `test isValid for input of invalid length`() {
        assertThrows(IllegalArgumentException::class.java) { bsnValidator.isValid("1234567") }
        assertThrows(IllegalArgumentException::class.java) { bsnValidator.isValid("1234567890") }
    }

    @Test
    fun `test isValid for non-numerical input`() {
        assertThrows(IllegalArgumentException::class.java) { bsnValidator.isValid("a11b22c33") }
        assertThrows(IllegalArgumentException::class.java) { bsnValidator.isValid("-111222333") }
        assertThrows(IllegalArgumentException::class.java) { bsnValidator.isValid("1234567.89") }
        assertThrows(IllegalArgumentException::class.java) { bsnValidator.isValid("1234567,89") }
        assertThrows(IllegalArgumentException::class.java) { bsnValidator.isValid("1.234567e8") }
    }


}
