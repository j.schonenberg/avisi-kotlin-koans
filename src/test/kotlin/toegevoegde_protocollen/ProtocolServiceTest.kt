package toegevoegde_protocollen

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


class ProtocolServiceTest {

    private val protocolService = ProtocolService()

    @Test
    fun `test getToegevoegdeProtocollen voor protocol zonder toevoeging`() {
        val protocol100 = Protocol(100)

        val result = protocolService.getToegevoegdeProtocolnummers(protocol100)
        assertEquals(0, result.size)
    }

    @Test
    fun `test getToegevoegdeProtocollen voor enkelvoudige toevoeging`() {
        val protocol100 = Protocol(100)
        val protocol200 = Protocol(200, listOf(protocol100))

        val result = protocolService.getToegevoegdeProtocolnummers(protocol200)
        assertEquals(listOf(100), result)
    }

    @Test
    fun `test getToegevoegdeProtocollen voor meervoudige toevoeging`() {
        val protocol100 = Protocol(100)
        val protocol200 = Protocol(200, listOf(protocol100))
        val protocol300 = Protocol(300, listOf(protocol200))
        val protocol400 = Protocol(400)
        val protocol500 = Protocol(500, listOf(protocol300, protocol400))

        val result = protocolService.getToegevoegdeProtocolnummers(protocol500)
        assertEquals(listOf(100, 200, 300, 400).sorted(), result.sorted())
    }

}
