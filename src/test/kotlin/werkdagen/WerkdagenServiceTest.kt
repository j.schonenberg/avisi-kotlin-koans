package werkdagen

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.time.LocalDate

internal class WerkdagenServiceTest {

    private val werkdagenService = WerkdagenService()

    val vrijdag_9_april_2021 = LocalDate.of(2021,4, 9)
    val maandag_12_april_2021 = LocalDate.of(2021, 4, 12)
    val vrijdag_16_april_2021 = LocalDate.of(2021,4, 16)
    val woensdag_28_april_2021 = LocalDate.of(2021, 4, 28)
    val dinsdag_18_mei_2021 = LocalDate.of(2021, 5, 18)

    @Test
    fun `test plusWerkdagen - binnen werkweek` () {
        assertEquals(vrijdag_16_april_2021, werkdagenService.plusWerkdagen(maandag_12_april_2021, 4))
    }

    @Test
    fun `test plusWerkdagen - normaliter in het weekend` () {
        assertEquals(maandag_12_april_2021, werkdagenService.plusWerkdagen(vrijdag_9_april_2021, 1))
    }

    @Test
    fun `test plusWerkdagen - over het weekend` () {
        assertEquals(vrijdag_16_april_2021, werkdagenService.plusWerkdagen(vrijdag_9_april_2021, 5))
    }

    @Test
    fun `test plusWerkdagen - over meerdere weken` () {
        assertEquals(dinsdag_18_mei_2021, werkdagenService.plusWerkdagen(woensdag_28_april_2021, 14))
    }

}
