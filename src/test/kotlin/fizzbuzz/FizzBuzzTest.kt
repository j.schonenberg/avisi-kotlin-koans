package fizzbuzz

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class FizzBuzzTest {

    private val game = FizzBuzz()

    @Test
    fun `test game`() {
        val result = game.play(1, 20)

        val expected = "1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, FizzBuzz, 16, 17, Fizz, 19, Buzz"
        assertEquals(expected, result)
    }

    @Test
    fun `test game with negative numbers`() {
        val result = game.play(-10, 10)

        val expected = "Buzz, Fizz, -8, -7, Fizz, Buzz, -4, Fizz, -2, -1, FizzBuzz, 1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz"
        assertEquals(expected, result)
    }

}
